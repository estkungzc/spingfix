import json
from urllib.request import urlopen
import ast
import re

def findInfoIp(ip):
    # url = 'http://ipinfo.io/{}/json'.format(ip)

    url = 'http://free.ipwhois.io/json/{}'.format(ip)
    response = urlopen(url)
    data = json.load(response)
    # print(data)
    return data

ip = "195.246.13.119"
# findInfoIp(ip)

def pas_ipv4():
    rex ="^(?:[0-9]{1,3}\.){3}[0-9]{1,3}$"
    data = [] # ipv4 list
    with open('log', mode='r') as log:
        for row in log:
            r = ast.literal_eval(row)

            if r['host'] not in [d for d in data]:
                match = re.match(rex, r['host'])
                if match:
                    data.append(match.group())
                    # print(match.group())
    return data


# output_file = open('loc', 'w', encoding='utf-8')

# for i in pas_ipv4():
#     json.dump(findInfoIp(i), output_file)
#     output_file.write("\n")