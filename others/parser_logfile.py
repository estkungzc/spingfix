import re
import json

# this pattern support for separate request


# pattern
# regex = '^(\S+) (\S+) (\S+) \[([\w:/]+\s[+-]\d{4})\] "(.*?)" (\d{3}) (\S+)'

def parser(log):
    regex = '^(\S+) (\S+) (\S+) \[([\w:/]+\s[+-]\d{4})\] "(\S+)?\s*(\S+)?\s*(\S+)?\s*" (\d{3}) (\S+)'
    match = re.search(regex, log)

    # support for case request is - (empty)
    match_not_none = list(filter(lambda x: x != None, match.groups()))

    format_log = ['host', 'identity_client',
                  'userid', 'time', 'request', 'status', 'size']
    sep = {} # for store log line
    index = -1
    for i, val in enumerate(match_not_none):

        # for check request not empty with concatenate the method, client request and protocol
        if format_log[index] == 'request' and len(match_not_none) > 7 and (4 < i < 7):
            sep[format_log[index]] += ' {}'.format(val)
            continue

        index += 1
        sep[format_log[index]] = val #assign key from format_log , value that matching

    result = "Host: {}, Identity of Client: {}, Userid: {}, Time: {}, Request: {}, Status: {}, Size: {}".format(
        sep['host'], sep['identity_client'], sep['userid'], sep['time'], sep['request'], sep['status'], sep['size'])
    return sep

lines = open('access_log.txt', 'r')
output_file = open('log', 'w', encoding='utf-8')

"""For display attributes log"""
for line in lines:
    # log_data.append(parser(line))
    json.dump(parser(line), output_file)
    output_file.write("\n")


"""For test function"""

# log = """64.242.88.10 - - [07/Mar/2004:16:05:49 +0800] "GET /twiki/bin/edit/Main/Double_bounce_sender?topicparent=Main.ConfigurationVariables HTTP/1.1" 401 12846"""
# log2 = """h194n2fls308o1033.telia.com - - [09/Mar/2004:13:49:05 -0800] "-" 408 -"""
# log3 = """10.0.0.153 - - [08/Mar/2004:10:48:02 -0800] "GET / HTTP/1.1" 304 -"""

# parser(log)
# parser(log2)
# parser(log3)
