from django.db import models

class Log(models.Model) :
    ip_addr = models.CharField(max_length=255)
    status = models.CharField(max_length=255)
    size = models.CharField(max_length=255)
