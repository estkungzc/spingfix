from django.shortcuts import render
from . import forms
import json
import ast
import re
import calendar
from operator import itemgetter
import datetime

def index(request):
    form = forms.FilterForm()
    log_list = []
    log_count = []
    filter_log = []
    display_log_list = []
    ip_count = []
    parser_access_log_list = []
    date_pattern = '([\d]+)-([\d]+)-([\d]+)'

    # date format 12/Mar/2004:13:04:01 -0800
    #log_date_pattern = '([\d]+)/([.*])/([\d]+):([\d]+):([\d]+):([\d]+) -([/d]+)'
    #ip_addr_pattern = '([\d\.]+)'

    with open('static/data/parsered_log.json', mode='r') as log:
        try:
            for row in log:
                r = ast.literal_eval(row)
                log_list.append(r)
        except :
            pass

    with open('static/data/parser_access_log.json', mode='r') as log:
        try:
            for row in log:
                r = ast.literal_eval(row)
                parser_access_log_list.append(r)
        except :
            pass


    open('static/data/log.json', 'w').close()
    open('static/data/parsered_log.json', 'w').close()

    log_count = log_file_counting(log_list)
    top10 = []
    for i in range(len(log_count)):
        top10.append(log_count[i])
        if i == 9 :
            break
    display_log_list = find_location(top10)

    mouth_dict = {k: v for k, v in enumerate(calendar.month_abbr)}

    ip_loc = []

    with open('static/data/access_log_location.json', mode='r') as loc:
        for row in loc:
            r = json.loads(row)

            if r["latitude"]:
                if r['ip'] in [d['host'] for d in display_log_list]:
                    # print(type(r["latitude"]))
                    selected = {
                        'ip': r['ip'],
                        'city': r["city"],
                        'lat': r["latitude"],
                        'lon': r["longitude"]
                    }
                    ip_loc.append(selected)

    if request.method == 'POST':
        form = forms.FilterForm(request.POST)
        if form.is_valid():
            filter_dict = {'ip_addr': 'host',
                           'status': 'status', 'size': 'size'}
            log_filter = str(form.cleaned_data['log_filter'])
            if log_filter != 'none':
                # filter by IP address, Status, Size
                input_filter = str(form.cleaned_data['search'])
                for item in parser_access_log_list:
                    if input_filter in str(item[filter_dict[log_filter]]):
                        filter_log.append(item)

            # pure_date = str(form.cleaned_data['start_date'])
            # split_date = re.match(date_pattern, pure_date).groups()
            # date = [split_date[2], mouth_dict[int(
            #     split_date[1])], split_date[0]]

            pure_start_date = str(form.cleaned_data['start_date'])
            split_start_date = re.match(date_pattern, pure_start_date).groups()
            start_date = [split_start_date[2],
                          split_start_date[1], split_start_date[0]]

            pure_stop_date = str(form.cleaned_data['stop_date'])
            split_stop_date = re.match(date_pattern, pure_stop_date).groups()
            stop_date = [split_stop_date[2],
                         split_stop_date[1], split_stop_date[0]]

            new_filter_log = []

            if log_filter == 'none':
                # print('NONE####################')
                # filter by date only
                for item in parser_access_log_list:
                    if is_log_date_in_range(item['time'], start_date, stop_date):
                        new_filter_log.append(item)
            else:
                # filter by date and attribute
                for item in filter_log:
                    if is_log_date_in_range(item['time'], start_date, stop_date):
                        new_filter_log.append(item)
            # print(len(new_filter_log))
            filter_log = new_filter_log
            filter_log_count = find_location(log_counting(filter_log))
            display_log_list = filter_log_count
            ip_loc = []
            with open('static/data/access_log_location.json', mode='r') as loc:
                for row in loc:
                    r = json.loads(row)

                    if r["latitude"]:
                        if r['ip'] in [d['host'] for d in filter_log_count]:
                            # print(type(r["latitude"]))
                            selected = {
                                'ip': r['ip'],
                                'city': r["city"],
                                'lat': r["latitude"],
                                'lon': r["longitude"]
                            }
                            ip_loc.append(selected)
    # print(ip_loc)
    context = {
        'text1': 'Hello world',
        'form': form,
        'data': display_log_list,
        'loc': ip_loc,
    }

    return render(request, 'index/index.html', context)


def log_file_counting(logList):
    '''
    count log function and keep it as dict and add to array
    '''
    log_list = logList
    ip_passed = []

    with open('static/data/ip_count.json', mode='r') as ip_count :
        try:
            for row in ip_count:
                r = ast.literal_eval(row)
                ip_passed.append(r)
        except :
            pass
    open('static/data/ip_count.json', 'w').close()

    for item in log_list:
        for i in ip_passed:
            if (item['host'] == i['host']) and (item['status'] == i['status']):
                i['count'] += 1
                print('hello')
                break
        else:
            # if len(log_list) == 1 and log_list[0]['host'] == '-' :
            #     a = {'host': item['host'], 'size': item['size'],
            #         'status': item['status'], 'count': 0}
            # else :
            a = {'host': item['host'], 'size': item['size'],
                'status': item['status'], 'count': 1}
            ip_passed.append(a)

    ip_passed = sorted(ip_passed, key=itemgetter('count'))
    ip_passed.reverse()

    output_file = open('static/data/ip_count.json', 'a', encoding='utf-8')

    for i in ip_passed:
        json.dump(i, output_file)
        output_file.write("\n")

    return ip_passed

def log_counting(logList):
    '''
    count log function and keep it as dict and add to array
    '''
    log_list = logList
    ip_passed = []

    for item in log_list:
        for i in ip_passed:
            if (item['host'] == i['host']) and (item['status'] == i['status']):
                i['count'] += 1
                break
        else:
            a = {'host': item['host'], 'size': item['size'],
                 'status': item['status'], 'count': 1}
            ip_passed.append(a)

    ip_passed = sorted(ip_passed, key=itemgetter('count'))
    ip_passed.reverse()

    return ip_passed


def split_log_date(logDate):
    '''
    read date and time from time attribute and filter only date
    '''
    mounth_dict = {v: k for k, v in enumerate(calendar.month_abbr)}
    split_date = []
    current = ''

    for i in logDate:
        if i == '/':
            split_date.append(current)
            current = ''
        elif i == ':':
            split_date.append(current)
            break
        else:
            current += i
    split_date[1] = mounth_dict[split_date[1]]
    return split_date


def is_log_date_in_range(logDate, startDate, stopDate):
    '''
    this function will check is the date in range
    '''
    log_date = split_log_date(logDate)
    date = datetime.date(int(log_date[2]), int(log_date[1]), int(log_date[0]))
    start_date = datetime.date(
        int(startDate[2]), int(startDate[1]), int(startDate[0]))
    stop_date = datetime.date(
        int(stopDate[2]), int(stopDate[1]), int(stopDate[0]))

    return start_date <= date <= stop_date


def find_location(logDict):
    log_dict = logDict
    ip_dict = []
    location_file = open('static/data/access_log_location.json', 'r')

    for row in location_file:
            r = json.loads(row)
            ip_dict.append(r)
    
    for i in log_dict:
        for r in ip_dict:
            if i['host'] == r['ip']:
                i['lat'] = r['latitude']
                i['lon'] = r['longitude']
                i['country'] = r['country']
                i['city'] = r['city']
    return log_dict

    
    