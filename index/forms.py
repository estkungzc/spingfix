from django import forms

log_choice = [
    ('none', ' '),
    ('ip_addr', 'IP address'),
    ('status', 'Status'),
    ('size', 'Size')
    ]
class FilterForm(forms.Form):
    start_date = forms.DateField(label='')
    stop_date = forms.DateField(label='')
    search = forms.CharField(max_length=255)
    log_filter = forms.CharField(label = 'Filter', widget=forms.Select(choices=log_choice))