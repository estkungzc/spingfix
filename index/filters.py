from index.models import Log
import django_filters

class LogFilter(django_filters.FilterSet):
    #ip_addr = django_filters.NumberFilter()

    class Meta:
        model = Log
        fields = ['ip_addr', 'status', 'size', ]