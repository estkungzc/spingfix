from django.urls import include, path
from log_files_manipulation import views

urlpatterns = [
    path('', views.manipulation, name='manipulation'),
    path('findIp', views.queryIp, name='queryIp')
]