from django.apps import AppConfig


class LogFilesManipulationConfig(AppConfig):
    name = 'log_files_manipulation'
