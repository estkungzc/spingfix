from django.shortcuts import render
from .forms import UploadFileForm
import re
import json
import ast
from urllib.request import urlopen

# Create your views here.

def manipulation(request):

    if request.method == 'POST':
        form = UploadFileForm(request.POST, request.FILES)
        if form.is_valid():
            data = []
            handle_uploaded_file(request.FILES['file'])

            output_file = open('static/data/parser_access_log.json', 'a', encoding='utf-8')

            open('static/data/parsered_log.json', 'w').close()
            output_file2 = open('static/data/parsered_log.json', 'a', encoding='utf-8')

            with open('static/data/log.json', mode='r') as log:
                for row in log:
                    result = parser(row)
                    data.append(result)
                    json.dump(result, output_file)
                    output_file.write("\n")

                    json.dump(result, output_file2)
                    output_file2.write("\n")
            
    else:
        data = ''
        form = UploadFileForm()

    context = {
        'form': form,
        'data': data
    }
    return render(request, 'manipulation/manipulation.html', context)

def queryIp(request):
    rex ="^(?:[0-9]{1,3}\.){3}[0-9]{1,3}$"

    counter = None
    count_log = 0
    data = [] # ipv4 list
    output_file = open('static/data/access_log_location.json', 'a', encoding='utf-8')
    
    with open('static/data/counter.json', mode='r') as k:
        for i in k:
            counter = int(i)
    with open('static/data/unique_ip.json', mode='r') as unique:
        for row in unique:
            r = json.loads(row)
            data.append(r)
    
    
    ip_loc = [] # for display data

    # print('init data is: ',data)
    # print('counter is: ', counter)

    
    with open('static/data/parser_access_log.json', mode='r') as parser_access_log:
        count_log = len(list(parser_access_log))

    if counter != count_log:
        with open('static/data/parser_access_log.json', mode='r') as parser_access_log:
            for row in list(parser_access_log)[counter:]:
                r = json.loads(row)
                print(r)
                if r['host'] not in [d for d in data]:
                    match = re.match(rex, r['host'])
                    if match:
                        data.append(match.group())
                        print(data)
                        loc = findInfoIp(match.group())
                        
                        ip_loc.append(loc)
                        json.dump(loc, output_file)
                        output_file.write("\n")
                counter += 1

        with open('static/data/counter.json', mode='w') as c:
            c.write(str(counter))
        with open('static/data/unique_ip.json', mode='w') as unique:
            for row in data:
                json.dump(row, unique)
                unique.write("\n")

    context = {
        'location': ip_loc
    }
    return render(request, 'findIp/findIp.html', context)

def handle_uploaded_file(f):
    with open('static/data/log.json', 'wb+') as destination:
        for chunk in f.chunks():
            destination.write(chunk)


def parser(log):
    regex = '^(\S+) (\S+) (\S+) \[([\w:/]+\s[+-]\d{4})\] "(\S+)?\s*(\S+)?\s*(\S+)?\s*" (\d{3}) (\S+)'
    match = re.search(regex, log)

    # support for case request is - (empty)
    match_not_none = list(filter(lambda x: x != None, match.groups()))

    format_log = ['host', 'identity_client',
                  'userid', 'time', 'request', 'status', 'size']
    sep = {} # for store log line
    index = -1
    for i, val in enumerate(match_not_none):

        # for check request not empty with concatenate the method, client request and protocol
        if format_log[index] == 'request' and len(match_not_none) > 7 and (4 < i < 7):
            sep[format_log[index]] += ' {}'.format(val)
            continue

        index += 1
        sep[format_log[index]] = val #assign key from format_log , value that matching

    return sep


def findInfoIp(ip):
    url = 'http://free.ipwhois.io/json/{}'.format(ip)
    response = urlopen(url)
    data = json.load(response)
    return data

def pas_ipv4(counter):
    count = counter
    rex ="^(?:[0-9]{1,3}\.){3}[0-9]{1,3}$"
    data = [] # ipv4 list
    with open('static/data/parser_access_log.json', mode='r') as log:
        for row in log:
            r = ast.literal_eval(row)

            if r['host'] not in [d for d in data]:
                match = re.match(rex, r['host'])
                if match:
                    data.append(match.group())
            count += 1
    return data,count